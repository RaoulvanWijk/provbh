Studentnummer:332491
Naam:Raoul

Verschilt de Terminal in Visual Studio Code met Git Bash?
	# het ziet er anders uit

Waar worden Branches vaak voor gebruikt?
	#  om veranderingen te zien

Hoe vaak ben je in Opdracht 5A van Branch gewisseld?
	# 2 keer

Vul de volgende commando's aan:
 -Checken op welke branch je aan het werken bent:
	# $ git branch
 -Nieuwe Branch aanmaken
	# $ git branch (naam van branch)
 -Van Branch wisselen
	 # $ git checkout (naam van branch)
 -Branch verwijderen
     # $ git branch -d (naam van branch)